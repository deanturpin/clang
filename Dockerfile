# Start with the latest OS and get it up-to-date
FROM ubuntu:devel AS build
RUN apt update
RUN apt full-upgrade --yes

# Install build dependencies
RUN apt-get update && \
    apt-get install --yes \
    build-essential \
    cmake \
    git \
    python3 \
    ninja-build

# Clone the LLVM project
RUN git clone --depth=1 https://github.com/llvm/llvm-project.git /llvm-project

# Create a build directory
WORKDIR /llvm-project/build

# Configure
RUN cmake -G Ninja \
          -DLLVM_ENABLE_PROJECTS=clang \
          -DCMAKE_BUILD_TYPE=Release \
          ../llvm

# Build and install
RUN ninja
RUN ninja install

FROM ubuntu:devel

COPY --from=build /usr/local/bin /usr/local/bin

# Install the essentials for dev
RUN apt update
RUN apt full-upgrade --yes
RUN apt install --yes git vim curl htop parallel tree tmux cmake locate time neofetch figlet mold
RUN apt install --yes libgtest-dev libbenchmark-dev libtbb-dev

CMD clang++ --version

